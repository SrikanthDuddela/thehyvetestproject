/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thehyvejob.thehyvejobtest.algorithms;

import de.thehyvejob.thehyvejobtest.Readers.ReadFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import javax.imageio.IIOException;

/**
 *
 * @author srd11
 *
 * decoding the input and re-encoding the decoded string
 */
public class DecodingAlgorithm {

    ReadFile rf;
    String decodeString;

    public DecodingAlgorithm(InputStream in) {
        try {
            rf = new ReadFile(in);
        } catch (Exception d) {
            System.out.println("Something went wrong with input " + d);
        }
    }

    public void StartDecoding() throws IOException {
        int available = rf.available();
        decodeString = "";
        SetupClass sc = new SetupClass();;
        while (available > 1) {
            sc = new SetupClass();
            int key = rf.readBytes();
            int value = rf.readBytes();
            if (key != -1) {
                sc.setCharKey(key);
                sc.setCharvalue(value);

                if (sc.getCharKey() == 0) {
                    decodeString = decodeString + sc.ConvertToString();
                    //    System.out.println(decodeString);
                } else {
                    int startIndex = (decodeString.length() - sc.getCharKey());
                    int stopIndex = startIndex + Integer.parseInt(sc.ConvertToString());
                    decodeString = decodeString + decodeString.substring(startIndex, stopIndex);
                    //  System.out.println(decodeString);

                }
            } else {
                break;

            }
        }

        sc.setEncode(decodeString);
        for (byte b : sc.getEncode()) {
            System.out.println(Arrays.toString(new byte[]{(byte) b}));
        }
        System.out.println("Final result: " + decodeString);
    }

    public void StartBetterDecoding() throws IOException {
        int available = rf.available();
        decodeString = "";
        int p = 0;
        int key = -1, value = -1;
        while (available > 1) {
            SetupClass sc = new SetupClass();
            if (p == 0) {
                key = rf.readBytes();
                if (Character.getNumericValue((char) key) != 0) {
                    value = key;
                    key = (byte) 48;
                } else {

                    value = rf.readBytes();
                }

            } else {
                key = rf.readBytes();

                value = rf.readBytes();
            }

            if (key != -1) {
                sc.setCharKey(key);
                sc.setCharvalue(value);

                if (sc.getCharKey() == 0) {
                    decodeString = decodeString + sc.ConvertToString();
                    // System.out.println(decodeString);
                } else {
                    int startIndex = (decodeString.length() - sc.getCharKey());
                    int stopIndex = startIndex + Integer.parseInt(sc.ConvertToString());
                    decodeString = decodeString + decodeString.substring(startIndex, stopIndex);
                    // System.out.println(decodeString);
                    sc.setEncode(decodeString);

                    for (byte b : sc.getEncode()) {
                        System.out.println(Arrays.toString(new byte[]{(byte) b}));
                    }

                }
            } else {
                break;
            }
            p = 1;
        }
        System.out.println("Final result: " + decodeString);
    }

}
