/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thehyvejob.thehyvejobtest.algorithms;

import java.util.ArrayList;

/**
 *
 * @author srd11
 */
public class SetupClass {

    int charKey;
    int charvalue;
    Boolean isChar;
    String encode;

    public ArrayList<Byte> getEncode() {

        ArrayList<Byte> usBytes = getUnsignedByte(encode.getBytes());
        return usBytes;
    }

    public ArrayList<Byte> getUnsignedByte(byte[] bytes) {
        ArrayList<Byte> ubytes;
        ubytes = new ArrayList<>();
        if (bytes.length > 0) {
            for (int i = 0; i < bytes.length; i++) {
                ubytes.add((byte) 0x30);
                ubytes.add((byte) (bytes[i] & 0xff));
            }
        } else {
            System.out.println("check your code");
        }
        return ubytes;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }

    public int getCharKey() {
        return charKey;
    }

    public void setCharKey(int charKey) {
        this.charKey = Character.getNumericValue((char) charKey);
    }

    public int getCharvalue() {
        return charvalue;
    }

    public void setCharvalue(int charvalue) {
        this.charvalue = charvalue;
    }

    public boolean isAscii(int ch) {
        return ch < 128;
    }

    public String ConvertToString() {
        
        if (charKey == 0) {
            if (isAscii(charvalue) == true) {
                return Character.toString((char) charvalue);
            } else {
                //ref:http://www.asciitable.com/
                this.setCharvalue(63);
                return "?";
            }
        } else {

            return Integer.toString(Character.getNumericValue((char) charvalue));
        }
    }
}
