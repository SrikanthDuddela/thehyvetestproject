package de.thehyvejob.thehyvejobtest.Test;

import de.thehyvejob.thehyvejobtest.algorithms.DecodingAlgorithm;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author srd11
 */
public class TestwithMainClass {

    public static void main(String[] args) throws IOException {
        try {
            File file = new File("C:\\Users\\srd11\\Documents\\NetBeansProjects\\TheHyveJobTest\\target\\testFile.txt");
            FileInputStream fin = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fin);
            DecodingAlgorithm decodeAlgo = new DecodingAlgorithm(in);
            int USE_TRIVIAL_IMPLEMENTATION = 1;
            if (USE_TRIVIAL_IMPLEMENTATION == 1) {
                decodeAlgo.StartBetterDecoding();
            } else {
                decodeAlgo.StartDecoding();
            }
        } catch (Exception a) {
            System.err.println("something went WRONG .. Check it in main() filestream" + a);

        }
    }

}
