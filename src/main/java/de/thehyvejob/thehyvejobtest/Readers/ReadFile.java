/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thehyvejob.thehyvejobtest.Readers;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author srd11
 *
 * This class takes file as an input and contains a function to convert data to
 * unsigned bytes
 * 
 * this class is an inspiration from http://codereview.stackexchange.com/questions/36884/critique-of-filterinputstream-and-filteroutputstream-classes
 */
public class ReadFile extends FilterInputStream {

    public ReadFile(InputStream in) {
        super(in);

    }
    
    
    public int readBytes() throws IOException {
        int b = in.read();
        return b;
    }
}
